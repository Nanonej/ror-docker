FROM ruby

WORKDIR /app

ENV PORT 3000

EXPOSE $PORT

RUN gem install rails bundler --no-document
RUN gem install rails --no-document

RUN apt-get update && apt-get install -y \
    nodejs \
    npm

RUN npm install --global yarn

ENTRYPOINT [ "/bin/bash" ]