# Ruby on Rails web development environment on Windows LTSC with Docker

## Requirement
- Check if CPU virtualization is enbaled in the BIOS
- Check if Hyper-V is enabled (Windows features)
- Install Docker

## Docker / Container setup

- Write a `.gitignore` and `.dockerignore` for the system

- Write a `Dockerfile` to create a RoR container
```
FROM ruby

WORKDIR /app

ENV PORT 3000

EXPOSE $PORT

RUN gem install rails bundler --no-document
RUN gem install rails --no-document

RUN apt-get update && apt-get install -y \
    nodejs \
    npm

RUN npm install --global yarn

ENTRYPOINT [ "/bin/bash" ]
```

- Write `docker-compose.yml` file, version 3.9 is the last released
```
version: "3.9"

services:
  ror_dev:
    build: .
    container_name: ror_container
    ports:
      - "3000:3000"
    volumes:
      - ./:/app
```

- Build the image with `docker-compose build`
- Start and enter the container with `docker-compose run --rm --service-ports ror_dev`

## Inside the container
- Generate a new app with `rails new my_app_name && cd my_app_name`
- Update and install gems with `bundle update && bundle install`
- Start your server `rails server -p $PORT -b 0.0.0.0` (stop with ctrl+c)

## Cleaning up
- When you're done exit the container and clean up container with `docker-compose down`